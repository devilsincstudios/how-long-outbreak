// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2015
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using System.Collections.Generic;

using DI.Core.Events;

namespace DI.Controllers
{
	[AddComponentMenu("Devil's Inc Studios/Controllers/Follow Camera")]
	public class FollowCamera : DI.Core.Behaviours.DI_MonoBehaviour
	{
		public Vector3 cameraOffset = new Vector3(0.0f, 9.0f, -4.0f);
		public List<GameObject> followTargets;
		public Vector3 midPoint;
		public bool updatingMidPoint = true;
		public Rigidbody localRigidBody;

		public void OnEnable()
		{
			DI_EventCenter<Collider>.addListener("OnTriggerEnter", handleOnTriggerEnter);
			DI_EventCenter<Collider>.addListener("OnTriggerExit", handleOnTriggerExit);
		}

		public void OnDisable()
		{
			DI_EventCenter<Collider>.removeListener("OnTriggerEnter", handleOnTriggerEnter);
			DI_EventCenter<Collider>.removeListener("OnTriggerExit", handleOnTriggerExit);
		}

		public void handleOnTriggerEnter(Collider other)
		{
			updatingMidPoint = false;
		}

		public void handleOnTriggerExit(Collider other)
		{
			updatingMidPoint = true;
		}

		public void LateUpdate()
		{
			// TODO actual math
			if (updatingMidPoint) {
				midPoint = Vector3.Lerp(followTargets[0].transform.position, followTargets[1].transform.position, 0.5f);
				localRigidBody.MovePosition(midPoint + cameraOffset);
			}
		}
	}
}
